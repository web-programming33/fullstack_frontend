import { ref, computed } from "vue";
import { defineStore } from "pinia";

export const useLoadingStore = defineStore("laoding", () => {
  const isLoading = ref(false);

  return { isLoading };
});

import type User from "@/types/User";
import http from "./axios";
function getUsers() {
  return http.get("/user");
}

function addNewUser(user: User) {
  return http.post("/user", user);
}

function updateUser(id: number, user: User) {
  return http.patch("/user/" + id, user);
}

function deleteUser(id: number) {
  return http.delete("/user/" + id);
}

export default { getUsers, addNewUser, updateUser, deleteUser };

export default interface User {
  id?: number;
  login: string;
  name: string;
  password: string;
  createdAT?: Date;
  updatedAT?: Date;
  deletedAT?: Date;
}
